from django.db import models

# Create your models here.
class Events(models.Model):
    NameEvent = models.CharField (max_length = 20)

    def __str__(self) :
        return self.NameEvent

    
class Participants(models.Model):
    NameEvent = models.ForeignKey(Events, on_delete=models.CASCADE) 
    Participant = models.CharField(max_length=20)

    def __str__(self) :
        return self.Participant + " - " + str(self.NameEvent)
