from django.contrib import admin
from .models import Events, Participants

# Register your models here.
admin.site.register(Events)
admin.site.register(Participants)