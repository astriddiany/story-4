from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.urls import reverse
from .forms import EventForm, PartForm
from .models import Events, Participants
from . import forms

# Create your views here.

def event(request):
    events = Events.objects.all()
    participants = Participants.objects.all()
    event_list = []
    for event in events :
        event_participant = []
        for participant in participants :
            if (participant.NameEvent.id == event.id) :
                event_participant.append(participant)
        event_list.append((event, event_participant))   

    context = {
        "events" : event_list,
        "participants" : participants,
    }
    return render(request, 'event_list.html', context)

def create (request) :
    if request.method == 'POST':
        form = forms.EventForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('event')
    else:
        form = forms.EventForm()
        return render(request, 'event.html', {'form': form})

def event_part (request,event_id) :
    participant_name = request.POST.get('participant_name', None)
    Participants.objects.create(
        Participant=participant_name,
        NameEvent=Events.objects.get(id=event_id)
    )
    return redirect(reverse('event'))
