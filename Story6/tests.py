from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .models import Events, Participants

# Create your tests here.
class TestEverything(TestCase):
    def test_url(self):
        response = Client().get('/event/')
        self.assertEquals(response.status_code, 200)
    
    def test_url_list(self):
        response = Client().get('/event/list/')
        self.assertEquals(response.status_code, 200)

    def test_template(self):
        response = Client().get('/event/')
        self.assertTemplateUsed(response, 'event.html')

    def test_template_list(self):
        response = Client().get('/event/list/')
        self.assertTemplateUsed(response, 'event_list.html')

    def test_model_createEvent(self):
        Events.objects.create(NameEvent="event_test")
        sum = Events.objects.all().count()
        self.assertEqual(sum,1)
    
    def test_model_createParticipant(self):
        event = Events.objects.create(NameEvent="event_test")
        Participants.objects.create(Participant="participant_test", NameEvent=event)
        sum = Participants.objects.all().count()
        self.assertEqual(sum,1)

    def test_model_event(self):
        Events.objects.create(NameEvent="event_test")
        event = Events.objects.get(NameEvent="event_test")
        self.assertEqual(str(event),"event_test")

    def test_model_participant(self):
        event = Events.objects.create(NameEvent="event_test")
        participant = Participants.objects.create(Participant="participant_test", NameEvent=event)
        self.assertEqual(str(participant),"participant_test - event_test")

    def test_save_event_Post(self):
        response = Client().post("/event/", data={"NameEvent":"event_test"})
        sum = Events.objects.all().count()
        self.assertEqual(sum,1)
        self.assertEqual(response.status_code,302)

    def test_save_participant_Post(self):
        Events.objects.create(NameEvent='event_test')
        response = Client().post('/event/list/', data={"event_test_id" : "1","participant" : "orang"})
        sum = Events.objects.all().count()
        self.assertEqual(sum,1)
        self.assertEqual(response.status_code,200)

    # def test_save_participant_Post(self):
    #     Events.objects.create(NameEvent='event_test')
    #     response = Client().post(reverse("event_part", kwargs={"event_id":1}), {"Participant":"participant_test", "NameEvent" : "event_test"})
    #     sum = Events.objects.all().count()
    #     count = Participants.objects.all().count()
    #     self.assertEqual(sum,1)
    #     self.assertEqual(count,1)
    #     self.assertEqual(response.status_code,200)

    def test_event_part(self):
        Events.objects.create(NameEvent='event_test')
        response = Client().post('/event/list/1', {"participant_name" : "tes"})
        sum = Events.objects.all().count()
        count = Participants.objects.all().count()
        self.assertEqual(sum,1)
        self.assertEqual(count,1)
        self.assertEqual(response.status_code,302)

    # def test_save_addParticipant_Post(self):
    #     Participants.objects.create(Participant='orang')
    #     Events.objects.create(NameEvent='event_test')
    #     response = Client().post('/event/list/1/', data={"Participant":"orang", "even_test":"ngoding"})
    #     sum = Events.objects.all().count()
    #     self.assertEqual(sum,1)
    #     self.assertEqual(response.status_code,200)

      