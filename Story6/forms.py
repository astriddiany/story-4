from django import forms
from .models import Events, Participants

class EventForm(forms.ModelForm):
    class Meta:
        model = Events
        fields = ['NameEvent']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })


class PartForm(forms.ModelForm):
    class Meta:
        model = Participants
        fields = ['NameEvent','Participant']