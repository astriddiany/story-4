from django.urls import path
from . import views

urlpatterns = [
     path('', views.create, name='create'),
     path('list/', views.event, name='event'),
     path('list/<int:event_id>', views.event_part, name='event_part'),
]