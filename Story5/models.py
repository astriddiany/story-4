from django.db import models

CATEGORY_CHOICES = (
    ('Odd 2019/2020', 'Odd 2019/2020'),
    ('Even 2019/2020','Even 2019/2020'),
    ('Odd 2020/2021', 'Odd 2020/2021'),
    ('Even 2020/2021', 'Even 2020/2021'),
    ('Odd 2021/2022', 'Odd 2021/2022'),
    ('Even 2021/2022', 'Even 2021/2022'),
)


class Matkul(models.Model):
    Subject = models.CharField(max_length = 20)
    Lecturer = models.CharField(max_length = 20)
    Credit = models.IntegerField()
    Desc = models.CharField(max_length=150)
    Semester = models.CharField (max_length = 20, choices=CATEGORY_CHOICES)
    Classroom = models.CharField(max_length = 20)


