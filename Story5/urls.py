from django.urls import path

from . import views

urlpatterns = [
    path('list/', views.matkuls, name='matkuls'),
    path('', views.matkul_create, name='matkul_create'),
    path('clear/<int:id>/', views.matkul_clear, name='matkul_clear'),
    path('detail/<int:id>/', views.matkul_detail, name="matkul_detail"),
]
