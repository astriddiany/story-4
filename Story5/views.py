from django.shortcuts import render, redirect
from .models import Matkul
from . import forms
from django.http import HttpResponse


def matkuls(request):
    list_matkul = Matkul.objects.all()
    return render(request, 'list_matkul.html', {'matkuls' :list_matkul})

def matkul_detail(request, id):
    if request.method == 'POST': #ini juga gausah
        matkulnya = Matkul.objects.filter(id=id).all() #gausah pake all
        return render(request, 'detail_matkul.html', {'matkul_detail' :matkulnya})

def matkul_create(request):
    if request.method == 'POST':
        form = forms.MatkulForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/schedule/list')
    else:
        form = forms.MatkulForm()
        return render(request, 'form_matkul.html', {'form': form})

def matkul_clear(request, id):
    if request.method == 'POST':
        Matkul.objects.filter(id=id).delete()
        return redirect('matkuls')
    else:
        return HttpResponse("/GET not allowed")
