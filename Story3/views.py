from django.shortcuts import render

def index(request):
    return render(request, 'page1.html')

def exp(request):
    return render(request, 'experiences.html')

def skills(request):
    return render(request, 'skills.html')

def contact(request):
    return (request, 'contact.html')